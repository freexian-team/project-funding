# Project Data

| Title | Debian Reimbursements Web App |
| ----- | ----- |
| **Funding source** | LTS |
| **Submitter** | Stefano Rivera <stefanor@debian.org> |
| **Executor** | Stefano Rivera <stefanor@debian.org> |
| **Reviewer** | Debian Treasurer Team <treasurer@debian.org> |
| **Proposal date** | 2022-11-15 |
| **Approval date** | 2022-11-24 |
| **Request for bids** | None, not needed. |
| **Project management issue** | https://salsa.debian.org/freexian-team/project-funding/-/issues/26 |
| **Completion date** | (insert date when completed) |

----------------------------------------

# Description

A web application to track planned expenses and reimbursements for the
Debian project (and similar organizations).
This replaces RT-based workflows and centralizes requests against all
TOs in one place.

## Project Details

A Django Application to track pre-approvals and reimbursement requests
for project members.
Ideally useful for other projects (e.g. SPI) but not necessarily
multi-tenant, it's reasonable to have one instance per project.

Requesters (project members) draft and submit requests for approval,
with a rough outline of the request (multiple lines of: type,
description, and amount).
A payer (TO) is selected by the system, based on the requester's country.

These requests are reviewed by the defined reviews and approvers (e.g.
the DPL).

Approved requests can be split into sub-requests (approved requests are
often split into expenses incurred by multiple project members), within
the approved budget.

Once the expenses have been incurred, the requester provides receipts
and descriptions of the expenses. (multiple lines of: date, description,
amount, currency, type).

If necessary, the requester can request an adjustment of the approval,
at this point. But if it's within a defined margin of the approved
amount, the payer (TO) can immediately refund the requester.

Large-scale sub-projects (e.g. DebConf) can batch load approved
sub-requests.

Some schema and flow documentation is in:
https://salsa.debian.org/stefanor/debian-reimbursement

## Completion Criteria

1. The application is deployed and has replaced the RT based
   reimbursement system. This implies acceptance from the treasurer team
   and the DPL.
1. There is sufficient test coverage to have a maintainable code-base
   (sadly a very subjective criterion)

## Estimated Effort

The project is divided into milestones for ease of estimation:

### Working prototype

This allows the basic request & refund flow to be exercised, but doesn't
have all the necessary details figured out, yet.

Expected to be missing/incomplete: Design, Email notifications, API
access, SSO integration, sub-requests, test coverage.

Estimated time to completion: (1 week - 40 hrs)

### Minimal Viable Product

This allows all the core flows to be exercised.
Testing this MVP is expected to expand the project scope and
requirements, a little.

Expected to be missing/incomplete: any parts of the project that are
proving difficult/complex to implement and are deferred.

Estimated time to completion: (1 week - 40 hrs)

### Deployable Product

This has enough issues resolved that it is ready to be deployed for
Debian.

Expected to be missing/incomplete: nothing. If anything proves too
difficult, the project scope must be updated to exclude it.

Estimated time to completion: (1 week - 40 hrs)

## Submitter Bid

Stefano has started work on this already, and is requesting that the
project be funded through Freexian's project funding, at Freexian's
usual customer rate.

The times estimated above are Stefano's best estimate from the work done
so far (27 hrs towards a working prototype).
