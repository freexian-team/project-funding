# Proposed checklist for LTS specific rules

This checklist is for moving a project from the idea milestone to the
proposed and then the accepted milestone and finally to completion.
There is a copy of this file in debian-lts/templates/ directory.

A list of the various milestones might be;

Idea --> proposed --> accepted --> execution --> completed

Once an idea is sumitted as a completed template, we move it into
"proposed" status. Once it is proposed it is open for questions
regarding it's time and cost estimate, its feasibility, etc. Then we
organise a vote among all paid LTS contributors. Ideally the polling
phase takes a week from announcement to completion. One can use
https://framadate.org or other similar service.

## Project funding checklist
### Proposal stage
 -[ ] Review merge request, keep as MR, add feedback in comments 
 -[ ] Send email to LTS about meeting time to review and vote on
      proposed project. 
 -[ ] Set up poll to accept or reject
 -[ ] Ensure project has reviewer
 -[ ] Ensure project has executor, if not use bidding process
 -[ ] Consider adding project to [web site](https://freexian-team.pages.debian.net/project-funding/projects/)  
#### Bidding stage
 -[ ] Create a new issue to track bids if necessary

### Acceptance stage
 -[ ] Create a new issue to track bids if necessary
### Execution stage
 -[ ] Create a new issue to track project execution and status
### Completion
 -[ ] Move project to the completed directory and close the issue


Please see: https://salsa.debian.org/freexian-team/project-funding/-/blob/master/Rules-LTS.md#how-will-project-proposals-be-approved

