# Project Proposal Instructions

This document provides guidance for completing each part of the project
proposal.

* Project Title - a brief title for the project

* Submitter - the name and contact information of the project submitter; if the
  team consists of more than one person, then include entries for all persons

* Description - a short (1 to 3 sentences) description of the project

* Project Details - a more thorough and detailed project description; may be
  several paragraphs long for large and complex projects

* Completion Criteria - the specific and measurable criteria that indicate
  successful project completion

* Estimated Effort - this section should include some estimation of how
  much work is involved to complete the project. To help come up with a
  reasonable estimation, it is expected that you split larger projects
  into separate milestones that would each have their own estimation.

* Submitter Bid - if the submitter wants to implement the project, they can 
  submit their own bid in that section. They should include a detailed 
  break down of the tasks and schedule information for the project execution. 
  The price offer can be included but if they want to keep it confidential, 
  they can send it by email to raphael@freexian.com
