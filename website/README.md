# Managing the Website Content

Freexian's [project funding web
site](https://freexian-team.pages.debian.net/project-funding/projects/)
is a simple static site, generated with Hugo, that directs interested persons to
information about proposing a new project or the status of current funded projects.

## Updating Project Information

As projects are proposed and move through the approval and execution phases,
information on the [project page](content/projects/index.md) should be updated.
Under normal circumstances, this should be the only page in the site that
requires updating.

Note that because the hugo site generator interprets links based on the base URL
of the static site rather than the Salsa project, links to issues in Salsa must
use a full URL, including hostname.

## Deploying the Site

At the recommendation of the Debian Salsa administrators, this site is rendered
locally, committed, then pushed to Git.  The Salsa CI service will deploy the
static content. Follow these steps when making updates to the site:

- Update the [project page](content/projects/index.md)
- Use `git diff` to verify that only expected changes are present
- Execute `hugo server` 
- Verify that the **Projects** page renders correctly and that any new or
  updated Salsa issue links lead to the expected location(s)
- `Ctrl+C` to exit `hugo server`
- Generate the site by executing `hugo`
- Use `git diff` to inspect the changes to the `public` directory; ensure that
  only expected changes are present
- Depending on the extent of the change, you may judge a merge request to be
  prudent rather than simply committing and pushing your changes without review
- There is a CI pipeline that runs once merged upon review or otherwise committed
